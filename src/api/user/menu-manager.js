import request from '@/utils/request'

export function treeMenu(data) {
  return request({
    url: '/role/treeMenu',
    method: 'post',
    data
  })
}

export function addMenu(data) {
  return request({
    url: '/menu/addMenu',
    method: 'post',
    data
  })
}

export function removeMenu(data) {
  return request({
    url: '/menu/removeMenu',
    method: 'post',
    data
  })
}

export function modifyMenu(data) {
  return request({
    url: '/menu/modifyMenu',
    method: 'post',
    data
  })
}
