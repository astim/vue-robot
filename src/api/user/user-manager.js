import request from '@/utils/request'

export function listUser(data) {
  return request({
    url: '/user/listUser',
    method: 'post',
    data
  })
}

export function removeUser(data) {
  return request({
    url: '/user/removeUser',
    method: 'post',
    data
  })
}

export function listRole(data) {
  return request({
    url: '/user/listRole',
    method: 'post',
    data
  })
}

export function listRoleByUserId(data) {
  return request({
    url: '/user/listRoleByUserId',
    method: 'post',
    data
  })
}

export function saveRoleByUserId(data) {
  return request({
    url: '/user/saveRoleByUserId',
    method: 'post',
    data
  })
}
export function addUser(data) {
  return request({
    url: '/user/addUser',
    method: 'post',
    data
  })
}

export function modifyUser(data) {
  return request({
    url: '/user/modifyUser',
    method: 'post',
    data
  })
}
