import Layout from '@/layout'

export const url = new Map()
// 用户
url.set('user', Layout)
url.set('userManager', () => import('@/views/user/user-manager/index'))
url.set('roleManager', () => import('@/views/user/role-manager/index'))
url.set('menuManager', () => import('@/views/user/menu-manager/index'))
// 机器人
url.set('robot', Layout)
url.set('robotConfig', () => import('@/views/pdf/index'))
url.set('robotManager', () => import('@/views/pdf/index'))
// 地图
url.set('map', Layout)
url.set('siteEdit', () => import('@/views/pdf/index'))
url.set('taskManager', () => import('@/views/pdf/index'))
// 应用
url.set('app', Layout)
url.set('authManager', () => import('@/views/app/auth-manager/index'))
